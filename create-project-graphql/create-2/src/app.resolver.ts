import { AppService } from './app.service';
import { Resolver, Query } from '@nestjs/graphql';
import {} from 'express';

@Resolver()
export class AppResolver {
  constructor(private readonly appService: AppService) {}

  @Query(() => String)
  async hello(): Promise<string> {
    return this.appService.getHello();
  }
}
