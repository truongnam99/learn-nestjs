import { CatService } from './cat.service';
import { Cat } from './dto/cat.type';
import { Resolver, Query, Args } from '@nestjs/graphql';

@Resolver(() => Cat)
export class CatResolver {
  constructor(private readonly catService: CatService) {}
  @Query(() => [Cat])
  getCats() {
    return this.catService.find();
  }

  @Query(() => Cat)
  getCat(@Args('name') name: string): Promise<Cat> {
    return this.catService.findOne(name);
  }
}
