import { Injectable, NotFoundException } from '@nestjs/common';
import { Cat } from './dto/cat.type';
const fake_Cats: Cat[] = [
  { name: 'abc', weight: 21 },
  { name: 'hii', weight: 22 },
  { name: 'hee', weight: 25 },
  { name: 'koo', weight: 22 },
];

@Injectable()
export class CatService {
  constructor() {}

  async findOne(name: string): Promise<Cat> {
    let rs = fake_Cats.filter(item => item.name === name);

    if (rs.length > 0) return rs[0];
    else throw new NotFoundException();
  }

  async find(): Promise<Cat[]> {
    return fake_Cats;
  }
}
