import { CatService } from './cat.service';
import { CatResolver } from './cat.resolver';
import { Module } from '@nestjs/common';

@Module({
  providers: [CatResolver, CatService],
})
export class CatModule {}
