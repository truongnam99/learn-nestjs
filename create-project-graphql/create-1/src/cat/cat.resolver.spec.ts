import { CatModule } from './cat.module';
import { Cat } from './dto/cat.type';
import { CatService } from './cat.service';
import { CatResolver } from './cat.resolver';
import { Test, TestingModule } from '@nestjs/testing';
describe('CatResoler', () => {
  let catResolver: CatResolver;

  beforeEach(async () => {
    const cat: TestingModule = await Test.createTestingModule({
      imports: [CatModule],
    }).compile();
    catResolver = cat.get<CatResolver>(CatResolver);
  });

  describe('getCats', () => {
    it('getCats should return Array of Cat', async () => {
      let cats = await catResolver.getCats();
      expect(Array.isArray(cats)).toBe(true);
      if (cats.length > 0) expect(cats[0] instanceof Cat);
    });
  });

  describe('getCat', () => {
    it('getCat should return Cat', async () => {
      let cat: Cat = await catResolver.getCat('abc');

      //   if (cat) expect(cat instanceof Cat).toBe(true); can't check

      if (cat) expect(typeof cat).toBe('object');
      expect(cat).toEqual({ name: 'abc', weight: 21 });
    });
  });
});
