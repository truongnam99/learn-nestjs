import { Field, ObjectType } from '@nestjs/graphql';

@ObjectType()
export class Cat {
  @Field()
  name: string;

  @Field()
  weight: number;
}
